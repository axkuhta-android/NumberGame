package com.example.numberguess

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val editUpper = findViewById<EditText>(R.id.editUpperBound)
        val editLower = findViewById<EditText>(R.id.editLowerBound)
        val button = findViewById<Button>(R.id.button)

        button.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)

            intent.putExtra("upper", editUpper.text.toString().toInt())
            intent.putExtra("lower", editLower.text.toString().toInt())

            startActivity(intent)
        }
    }
}