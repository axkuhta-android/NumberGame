package com.example.numberguess

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import java.lang.Math.random
import kotlin.math.roundToInt

class GameActivity : AppCompatActivity() {
    var upper:Int = 100
    var lower:Int = 0
    var guess:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        upper = intent.getIntExtra("upper", 100)
        lower = intent.getIntExtra("lower", 0)

        val much = findViewById<Button>(R.id.muchButton)
        val little = findViewById<Button>(R.id.littleButton)
        val back = findViewById<TextView>(R.id.backButton)

        much.setOnClickListener {
            handleMuch()
        }

        little.setOnClickListener {
            handleLittle()
        }

        back.setOnClickListener {
            finish()
        }

        makeGuess()
    }

    fun handleMuch() {
        if (guess == lower) {
            confused()
        } else {
            upper = guess - 1
            makeGuess()
        }
    }

    fun handleLittle() {
        if (guess == upper) {
            confused()
        } else {
            lower = guess + 1
            makeGuess()
        }
    }

    // Triggered on inputs like:
    // 91 Too much
    // 90 Too little
    fun confused() {
        val guessText = findViewById<TextView>(R.id.guessText)
        val much = findViewById<Button>(R.id.muchButton)
        val little = findViewById<Button>(R.id.littleButton)
        val back = findViewById<TextView>(R.id.backButton)

        guessText.text = "...Uhhh?"
        much.isVisible = false
        little.isVisible = false
        back.isVisible = true
    }

    fun makeGuess() {
        val guessText = findViewById<TextView>(R.id.guessText)
        val much = findViewById<Button>(R.id.muchButton)
        val little = findViewById<Button>(R.id.littleButton)

        val delta = upper - lower

        if (delta > 1) {
            guess = lower + (random() * delta).roundToInt()
            guessText.text = "My guess is ${guess}"
        } else if (delta == 1){
            guessText.text = "I'm at a loss. Which one is it?"
            much.text = "It's ${upper}"
            little.text = "It's ${lower}"

            much.setOnClickListener {
                finish()
            }

            little.setOnClickListener {
                finish()
            }
        } else {
            val back = findViewById<TextView>(R.id.backButton)

            guessText.text = "I know it's ${upper}"
            much.isVisible = false
            little.isVisible = false
            back.isVisible = true
        }
    }
}